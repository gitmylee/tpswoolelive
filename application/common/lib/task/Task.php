<?php

namespace app\common\lib\task;
use app\common\lib\SendSms;
use app\common\lib\redis\Predis;
use app\common\lib\Redis;

class Task
{
    /**
     * 发送短信验证码task
     * @param $data
     * @param $serv swoole server对象
     */
    public function sendSms($data,$serv){
        try {
            $result = SendSms::sendCode($data['phone_number'], $data['code']);
        } catch (\Exception $exception) {
           return false;
        }
        if ($result){
            Predis::getInstance()->set(Redis::smsKey($data['phone_number']),$data['code'],config('redis.exp'));
        }else{
            return false;
        }
        return true;
    }

    /**
     * 通过task机制发送赛事数据
     * @param $data
     * @param $serv
     */
    public function pushLive($data,$serv){
        $user = Predis::getInstance()->redis->sMembers(config('redis.live_game_key'));
        foreach ($user as $k => $fd) {
            $serv->push($fd, json_encode($data));
        }
    }


    /**
     * 聊天TASK
     * @param $data
     * @param $serv
     */
    public function chatPush($data,$serv){
        foreach ($serv->ports[1]->connections as $fd){
            $serv->push($fd,json_encode($data));
        }
    }
}