<?php


namespace app\common\lib\redis;


class Predis
{
    public $redis = '';
    private static $_insrance = null;

    public static function getInstance()
    {
        if (empty(self::$_insrance)) {
            return self::$_insrance = new self();
        } else {
            return self::$_insrance;
        }
    }

    private function __construct()
    {
        $this->redis = new \Redis();
        $result = $this->redis->connect(config('redis.host'), config('redis.port'), config('redis.timeOut'));
        if ($result === false) {
            throw new \Exception('redis connect error');
        }
    }

    /**
     * 设置redis
     * @param $key
     * @param $value
     * @param $time
     */
    public function set($key, $value, $time = 0)
    {
        if (!$key) {
            return '';
        }
        if (is_array($value)) {
            $value = json_encode($value);
        }
        if (!$time) {
            return $this->redis->set($key, $value);
        }
        return $this->redis->setex($key, $time, $value);
    }

    /**
     * 获取redis
     * @param $key
     */
    public function get($key)
    {
        if (!$key) {
            return '';
        }
        return $this->redis->get($key);
    }

    /**
     * 删除用户验证码
     * @param $key
     * @return string
     */
    public function delSms($key)
    {
        if (!$key) {
            return '';
        }
        $this->redis->del($key);
    }

    /**
     * 添加用户到redis集合
     * @param $key
     * @param $value
     * @return int
     */
//    public function sadd($key, $value)
//    {
//        return $this->redis->sAdd($key, $value);
//    }


    /**
     * 删除集合中用户
     * @param $key
     * @param $value
     * @return int
     */
//    public function srem($key,$value)
//    {
//        return $this->redis->sRem($key,$value);
//    }

    /**
     *
     * @param $name
     * @param $arguments
     */
    public function __call($name,$arguments){
        if (count($arguments)!=2){
            return '';
        }
        return $this->redis->$name($arguments[0],$arguments[1]);
    }
}