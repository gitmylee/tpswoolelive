<?php

namespace app\common\lib;

class Util
{
	static function show($status,$msg='',$data=[]){
		$data = [
			'status'=>$status,
			'msg'=>$msg,
			'data'=>$data,
		];
		echo json_encode($data);
	}
}