<?php
namespace app\index\controller;

use think\Controller;
use think\Request;
use app\common\lib\Util;

class Chat extends Controller
{
    public function index(Request $request)
    {
        if (empty($_POST['game_id'])){
            return Util::show(config('code.error'),'error1');
        }
        if (empty($_POST['content'])){
            return Util::show(config('code.error'),'error2');
        }
        $data =[
            'user'=>'用户'.rand(1,2000),
            'content'=>$_POST['content']
        ];

        $taskdata = [
            'method' => 'chatPush',
            'data'=>$data
        ];
        $_POST['http_server']->task($taskdata);
        return Util::show(config('code.success'), 'send ok');

    }
}
