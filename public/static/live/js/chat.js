var wsUrl = "ws://oldpub.cn:9502";

var websocket = new WebSocket(wsUrl);

//实例对象的onopen属性
websocket.onopen = function (evt) {
    // websocket.send("hello-sinwa");
    console.log("conected-swoole-success");
}

// 实例化 onmessage
websocket.onmessage = function (evt) {
    push(evt.data);
    // console.log("ws-server-return-data:" + evt.data);
}

//onclose
websocket.onclose = function (evt) {
    console.log("close");
}
//onerror

websocket.onerror = function (evt, e) {
    console.log("error:" + evt.data);
}

function push(data) {
    // console.log(data);
    data = JSON.parse(data);
    var html = '<div class="comment">'+'<span>'+data.user+'</span>'+'<span>'+data.content+'</span>'+'</div>';
    $('#comments').append(html);

}