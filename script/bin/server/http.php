<?php


use think\Container;

class Http
{
    CONST HOST = '0.0.0.0';
    CONST PORT = 9501;

    public $http = null;

    public function __construct()
    {
        $this->http = new swoole_http_server(self::HOST, self::PORT);
        $this->http->set([
            'enable_static_handler' => true,
            'document_root' => '/www/wwwroot/live/public/static',
            'worker_num' => 2,
            'task_worker_num' => 2,
        ]);

        $this->http->on("workerstart", [$this, 'onWorkerStart']);
        $this->http->on("request", [$this, 'onRequest']);
        $this->http->on("task", [$this, 'onTask']);
        $this->http->on("finish", [$this, 'onFinish']);
        $this->http->on("close", [$this, 'onClose']);

        $this->http->start();
    }

    /**
     * @param $server
     * @param $worker_id
     */
    public function onWorkerStart($server, $worker_id)
    {
        // 定义应用目录
        define('APP_PATH', __DIR__ . '/../application/');
        // 加载框架引导文件
//        require __DIR__ . '/../thinkphp/base.php';
        require __DIR__ . '/../thinkphp/start.php';
    }

    public function onRequest($request, $response)
    {
//        $_SERVER = [];
        if (isset($request->server)) {
            foreach ($request->server as $k => $v) {
                $_SERVER[strtoupper($k)] = $v;
            }
        }
//        var_dump($_SERVER);
        if (isset($request->header)) {
            foreach ($request->header as $k => $v) {
                $_SERVER[strtoupper($k)] = $v;
            }
        }
        $_GET = [];
        if (isset($request->get)) {
            foreach ($request->get as $k => $v) {
                $_GET[$k] = $v;
            }
        }
        $_POST = [];
        if (isset($request->post)) {
            foreach ($request->post as $k => $v) {
                $_POST[$k] = $v;
            }
        }
        $_POST['http_server'] = $this->http;
//        var_dump($_POST);
        ob_start();
        try {
            // 进入框架执行应用并响应
            think\Container::get('app')->run()->send();
        } catch (\Exception $exception) {
            //todo
        }
        $res = ob_get_contents();
        ob_end_clean();
//	ob_end_clean();
//        if (ob_get_length() > 0) {
//            ob_end_clean();
//        }
        $response->end($res);
    }

    /**
     * @param $serv
     * @param $taskId
     * @param $workerId
     * @param $data
     */
    public function onTask($serv, $taskId, $workerId, $data)
    {
        $obj = new app\common\lib\task\Task;
        $method = $data['method'];
        $res = $obj->$method($data['data']);
        /*
        try {
            var_dump($data);
            $result = app\common\lib\SendSms::sendCode($data['phone_number'], $data['code']);
        } catch (\Exception $exception) {
            echo $exception->getMessage();
//            return Util::show(config('code.error'), $exception->getMessage());
        }
        var_dump($result);
        */
//        if ($res) {
//            $redis = new \Swoole\Coroutine\Redis();
//            $redis->connect(config('redis.host'), config('redis.port'));
//            $redis->set(Redis::smsKey($data['phone_number']), $data['code'], config('redis.exp'));
////			return json(['status'=>1,'msg'=>'success']);
//        }
        return $res;
//        return "on task finish";
    }

    /**
     * @param $serv
     * @param $taskId
     * @param $data
     */
    public function onFinish($serv, $taskId, $data)
    {
        echo "taskId:{$taskId}\n";
        echo "finish-data-sucess:{$data}\n";
    }

    /**
     * close
     * @param $ws
     * @param $fd
     */
    public function onClose($ws, $fd)
    {
        echo "clientid:{$fd}\n";
    }

}

new Http();