<?php


//use think\Container;
//use app\common\lib\redis\Predis;
use Swoole\Coroutine as co;

class Ws
{
    CONST HOST = '0.0.0.0';
    CONST PORT = 9501;
    CONST CHAT_PORT = 9502;

    public $ws = null;

    public function __construct()
    {
        //获取redis集合用户的值 如果有 就删除
//        if (is_array($user = app\common\lib\redis\Predis::getInstance()->redis->sMembers(config('redis.live_game_key')))){
//            foreach ($user as $fd){
//                Predis::getInstance()->redis->sRem(config('redis.live_game_key'),$fd);
//            }
//        }
        $this->redis = new \Redis();
        $result = $this->redis->connect('127.0.0.1', 6379, 5);
        if ($result === false) {
            throw new \Exception('redis connect error');
        }

        if (is_array($user = $this->redis->sMembers('live_game_key'))){
            foreach ($user as $fd){
                $this->redis->sRem('live_game_key',$fd);
            }
        }
        $this->ws = new swoole_websocket_server(self::HOST, self::PORT);
        $this->ws->listen(self::HOST,self::CHAT_PORT,SWOOLE_SOCK_TCP);
        $this->ws->set([
            'enable_static_handler' => true,
            'document_root' => '/www/wwwroot/live/public/static',
            'worker_num' => 2,
            'task_worker_num' => 2,
        ]);

        $this->ws->on("open", [$this, 'onOpen']);
        $this->ws->on("message", [$this, 'onMessage']);
        $this->ws->on("workerstart", [$this, 'onWorkerStart']);
        $this->ws->on("request", [$this, 'onRequest']);
        $this->ws->on("task", [$this, 'onTask']);
        $this->ws->on("finish", [$this, 'onFinish']);
        $this->ws->on("close", [$this, 'onClose']);

        $this->ws->start();
    }

    /**
     * @param $server
     * @param $worker_id
     */
    public function onWorkerStart($server, $worker_id)
    {
        // 定义应用目录
        define('APP_PATH', __DIR__ . '/../../../application/');
        // 加载框架引导文件
//        require __DIR__ . '/../thinkphp/base.php';
        require __DIR__ . '/../../../thinkphp/start.php';
    }

    public function onRequest($request, $response)
    {
//        $_SERVER = [];
        if (isset($request->server)) {
            foreach ($request->server as $k => $v) {
                $_SERVER[strtoupper($k)] = $v;
            }
        }
//        var_dump($_SERVER);
        if (isset($request->header)) {
            foreach ($request->header as $k => $v) {
                $_SERVER[strtoupper($k)] = $v;
            }
        }
        $_GET = [];
        if (isset($request->get)) {
            foreach ($request->get as $k => $v) {
                $_GET[$k] = $v;
            }
        }
        $_POST = [];
        if (isset($request->post)) {
            foreach ($request->post as $k => $v) {
                $_POST[$k] = $v;
            }
        }
        $_FILES = [];
        if (isset($request->files)) {
            foreach ($request->files as $k => $v) {
                $_FILES[$k] = $v;
            }
        }
        $this->writeLog();
        $_POST['http_server'] = $this->ws;
//        var_dump($_POST);
        ob_start();
        try {
            // 进入框架执行应用并响应
            think\Container::get('app')->run()->send();
        } catch (\Exception $exception) {
            //todo
        }
        $res = ob_get_contents();
        ob_end_clean();
//	ob_end_clean();
//        if (ob_get_length() > 0) {
//            ob_end_clean();
//        }
        $response->end($res);
    }

    /**
     * @param $serv
     * @param $taskId
     * @param $workerId
     * @param $data
     */
    public function onTask($serv, $taskId, $workerId, $data)
    {
        $obj = new app\common\lib\task\Task;
        $method = $data['method'];
        $res = $obj->$method($data['data'],$serv);
        /*
        try {
            var_dump($data);
            $result = app\common\lib\SendSms::sendCode($data['phone_number'], $data['code']);
        } catch (\Exception $exception) {
            echo $exception->getMessage();
//            return Util::show(config('code.error'), $exception->getMessage());
        }
        var_dump($result);
        */
//        if ($res) {
//            $redis = new \Swoole\Coroutine\Redis();
//            $redis->connect(config('redis.host'), config('redis.port'));
//            $redis->set(Redis::smsKey($data['phone_number']), $data['code'], config('redis.exp'));
////			return json(['status'=>1,'msg'=>'success']);
//        }
        return $res;
//        return "on task finish";
    }

    /**
     * @param $serv
     * @param $taskId
     * @param $data
     */
    public function onFinish($serv, $taskId, $data)
    {
        echo "taskId:{$taskId}\n";
        echo "finish-data-sucess:{$data}\n";
    }

    /**
     * 监听ws连接事件
     * @param $ws
     * @param $request
     */
    public function onOpen($ws, $request) {
//        print_r($ws);
        var_dump($request->fd);
        \app\common\lib\redis\Predis::getInstance()->sadd(config('redis.live_game_key'),$request->fd);
    }

    /**
     * 监听ws消息事件
     * @param $ws
     * @param $frame
     */
    public function onMessage($ws, $frame) {
        echo "ser-push-message:{$frame->data}\n";
//        $ws->push($frame->fd, "server-push:".date("Y-m-d H:i:s"));
    }

    /**
     * close
     * @param $ws
     * @param $fd
     */
    public function onClose($ws, $fd)
    {
        \app\common\lib\redis\Predis::getInstance()->srem(config('redis.live_game_key'),$fd);
        echo "clientid:{$fd}\n";
    }

    public function writeLog(){
        $datas = array_merge(['data'=>date('Ymd H:i:s')],$_GET,$_POST,$_SERVER);
        $logs = '';
        foreach ($datas as $key=>$value){
            if (is_array($value)){
                $value = json_encode($value);
                $logs .= $key . ":" . $value . '';
            }else{
                $logs .= $key . ":" . $value . '';
            }

        }
        $logs .= PHP_EOL;
        //利用异步文件I/O写进日志(已经废弃)
//        swoole_async_writefile(APP_PATH.'../runtime/log/'.date('Ym').'/'.date('d').'access.log',$logs.PHP_EOL,function(){
//            //todo
//        },FILE_APPEND);
        //swoole 协程写进日志文件
        $filename = APP_PATH.'../runtime/log/'.date('Ym').'/'.date('d').'_access.log';
        co::create(function () use ($filename,$logs)
        {
            $r =  co::writeFile($filename,$logs,FILE_APPEND);
            var_dump($r);
        });

    }

}

new Ws();